# Car Management API

Car Management API is Challenge Chapter 6 given by Binar Academy.

To complete the requirements for making a challenge, you can see the link below :

- Express.Js `https://expressjs.com/`
- Sequelize `https://sequelize.org/`
- Postgresql `https://www.postgresql.org/docs/`
- Bcrypt `https://www.npmjs.com/package/bcrypt`
- JSON Web Token `https://jwt.io/introduction/`
- Swagger `https://swagger.io/docs/specification/about/`


## Super Admin Account

- Email `lucas@gmail.com`
- Password `lucas1234`

## Endpoint API Documentation

Here's the REST API Documentation endpoint using Swagger:

- Localhost `http://localhost:2000/`

**Example:**

```
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
```

```
http://localhost:2000/api-docs
```

## Endpoint Registrasi Member

**Example:**
```
/auth/register
```

```
http://localhost:2000/api-docs/#/Auth/post_auth_register
```


## Endpoint Login All Roles

**Example:**
```
/auth/login
```

```
http://localhost:2000/api-docs/#/Auth/post_auth_login
```

## Endpoint Registrasi Admin

**Example:**
```
/auth/admin/register
```

```
http://localhost:2000/api-docs/#/Auth/post_auth_admin_register
```

## Endpoint Current User

**Example:**
```
/auth/me
```

```
http://localhost:2000/api-docs/#/Auth/get_auth_me
```


## Endpoint Get All Cars

**Example:**
```
/cars
```

```
http://localhost:2000/api-docs/#/Cars/get_cars
```

## Endpoint Create Car Data

**Example:**
```
/cars/add
```

```
http://localhost:2000/api-docs/#/Cars/post_cars_add
```

## Endpoint Update Car Data

**Example:**
```
/cars/update/{id}
```

```
http://localhost:2000/api-docs/#/Cars/put_cars_update__id_
```

## Endpoint Delete Car Data

**Example:**
```
/cars/delete/{id}
```

```
http://localhost:2000/api-docs/#/Cars/delete_cars_delete__id_
```


## Endpoint Get Cars By Available

**Example:**
```
/cars/available?
```

```
http://localhost:2000/api-docs/#/Cars/get_cars_available_
```
