const express = require("express");
const bodyParser = require("body-parser");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./swagger.json");
const cors = require("cors");

const app = express();
const PORT = 2000;

app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

// ================  Import Controllers  ======================//
const authController = require("./controllers/authController");
const carsController = require("./controllers/carsController");


// =========  Import Middlewares  ================ //
const middleware = require("./middlewares/auth");

// ===============    Define Routes  =================//

// --------------- Auth ------------------------ //

// Auth Register Member 
app.post("/auth/register", authController.register);

// Auth Register Admin
app.post(
    "/auth/admin/register", 
    middleware.authenticate, 
    middleware.isSuperAdmin, 
    authController.register
);

// Auth Login Member, Admin, SuperAdmin
app.post("/auth/login", authController.login);

// Auth Login With Google
app.post("/auth/login-google", authController.loginGoogle);

// Auth Current User
app.get("/auth/me", middleware.authenticate, authController.currentUser);

// --------------- End Auth ------------------------ //


// --------------- Cars  ------------------------ //

// Get All Cars
app.get(
    "/cars", 
    middleware.authenticate,
    carsController.getAll
);


// Create Car
app.post(
    "/cars/add", 
    middleware.authenticate, 
    middleware.exclusiveUser,
    carsController.createCar
);

// Update Car
app.put(
    "/cars/update/:id", 
    middleware.authenticate, 
    middleware.exclusiveUser,
    carsController.updateCar
);

// Delete Car
app.delete(
    "/cars/delete/:id", 
    middleware.authenticate, 
    middleware.exclusiveUser,
    carsController.deleteCar
);

// Available Car
app.get("/cars/available?", middleware.authenticate, carsController.availableCar);

// --------------- End Cars ------------------------ //


// --------------- API Documentation ------------------------ //

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// --------------- End API Documentation ------------------------ //




app.listen(PORT, () => {
    console.log(`Server listen on http://localhost:${PORT}`);
});
