{
    "swagger": "2.0",
    "info": {
        "version": "1.0.0",
        "title": "Car Management API",
        "description": "Challenge Chapter 6 - Binar Academy",
        "license": {
            "name": "MIT",
            "url": "https://opensource.org/licenses/MIT"
        }
    },
    "host": "localhost:2000",
    "basePath": "/",
    "tags": [{
            "name": "Auth",
            "description": "Authentication API"
        },
        {
            "name": "Cars",
            "description": "Cars Data API"
        }
    ],
    "schemes": ["http"],
    "consumes": ["application/json"],
    "produces": ["application/json"],
    "paths": {
        "/auth/register": {
            "post": {
                "tags": ["Auth"],
                "summary": "Register Member Endpoint",
                "parameters": [{
                    "name": "register",
                    "in": "body",
                    "description": "Register Member",
                    "schema": {
                        "$ref": "#/definitions/Register"
                    }
                }],
                "responses": {
                    "201": {
                        "description": "Success registered member."
                    }
                }
            }
        },
        "/auth/login": {
            "post": {
                "tags": ["Auth"],
                "summary": "Login All Roles Endpoint",
                "parameters": [{
                    "name": "Login",
                    "in": "body",
                    "description": "Login All Roles",
                    "schema": {
                        "$ref": "#/definitions/Login"
                    }
                }],
                "responses": {
                    "201": {
                        "description": "Success Login."
                    }
                }
            }
        },
        "/auth/admin/register": {
            "post": {
                "tags": ["Auth"],
                "summary": "Register Admin Endpoint",
                "security": [{
                    "Bearer": []
                }],
                "parameters": [{
                    "name": "Register Admin",
                    "in": "body",
                    "description": "Register admin by super admin",
                    "schema": {
                        "$ref": "#/definitions/Register"
                    }
                }],
                "responses": {
                    "201": {
                        "description": "Success registered Admin."
                    }
                }
            }
        },
        "/auth/me": {
            "get": {
                "tags": ["Auth"],
                "summary": "Get User with token Endpoint",
                "security": [{
                    "Bearer": []
                }],
                "responses": {
                    "200": {
                        "description": "Data has been obtained",
                        "schema": {
                            "$ref": "#/definitions/Register"
                        }
                    }
                }
            }
        },

        "/cars": {
            "get": {
                "tags": ["Cars"],
                "summary": "Get All Cars Endpoint",
                "security": [{
                    "Bearer": []
                }],
                "responses": {
                    "200": {
                        "description": "Success get all cars",
                        "schema": {
                            "$ref": "#/definitions/CarsData"
                        }
                    }
                }
            }
        },
        "/cars/add": {
            "post": {
                "tags": ["Cars"],
                "summary": "Create Car Endpoint",
                "security": [{
                    "Bearer": []
                }],
                "parameters": [{
                    "name": "Create Car",
                    "in": "body",
                    "description": "Create car data",
                    "schema": {
                        "$ref": "#/definitions/CarsData"
                    }
                }],
                "responses": {
                    "201": {
                        "description": "Success Created Cars."
                    }
                }
            }
        },
        "/cars/update/{id}": {
            "parameters": [{
                "name": "Update Car Endpoint",
                "in": "path",
                "required": true,
                "description": "Id of the car you want to change",
                "type": "string"
            }],
            "put": {
                "tags": ["Cars"],
                "summary": "Update Car Endpoint",
                "security": [{
                    "Bearer": []
                }],
                "parameters": [{
                    "name": "Update Car Data",
                    "in": "body",
                    "description": "Updated car",
                    "schema": {
                        "$ref": "#/definitions/CarsData"
                    }
                }],
                "responses": {
                    "201": {
                        "description": "Success Updated Cars."
                    }
                }
            }
        },
        "/cars/delete/{id}": {
            "delete": {
                "tags": ["Cars"],
                "summary": "Delete Car Endpoint",
                "security": [{
                    "Bearer": []
                }],
                "parameters": [{
                    "name": "id",
                    "in": "path",
                    "required": true,
                    "description": "Id of the car you want to delete",
                    "type": "string"
                }],
                "responses": {
                    "201": {
                        "description": "Success Deleted Cars."
                    }
                }
            }
        },
        "/cars/available?": {
            "get": {
                "tags": ["Cars"],
                "summary": "Available Cars Endpoint",
                "parameters": [{
                    "name": "Available Cars",
                    "in": "body",
                    "description": "Available Cars Data",
                    "schema": {
                        "$ref": "#/definitions/Filter"
                    }
                }],
                "responses": {
                    "201": {
                        "description": "Success filtered Cars.",
                        "schema": {
                            "$ref": "#/definitions/CarsData"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "Register": {
            "properties": {
                "name": {
                    "type": "string"
                },
                "email": {
                    "type": "string"
                },
                "password": {
                    "type": "string"
                },
                "role": {
                    "type": "string"
                }
            }
        },
        "Login": {
            "properties": {
                "email": {
                    "type": "string"
                },
                "password": {
                    "type": "string"
                }
            }
        },
        "CarsData": {
            "properties": {
                "plate": {
                    "type": "string"
                },
                "manufacture": {
                    "type": "string"
                },
                "model": {
                    "type": "string"
                },
                "image": {
                    "type": "string"
                },
                "rentPerDay": {
                    "type": "integer"
                },
                "capacity": {
                    "type": "integer"
                },
                "description": {
                    "type": "string"
                },
                "transmission": {
                    "type": "string"
                },
                "type": {
                    "type": "string"
                },
                "year": {
                    "type": "string"
                },
                "options": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "specs": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "availableAt": {
                    "type": "string"
                },
                "isWithDriver": {
                    "type": "boolean"
                }
            }
        },
        "Filter": {
            "properties": {
                "availableAt": {
                    "type": "string"
                },
                "isWithDriver": {
                    "type": "boolean"
                }
            }
        }
    },
    "securityDefinitions": {
        "Bearer": {
            "type": "apiKey",
            "name": "Authorization",
            "in": "header",
            "description": "Enter your bearer token in the format **Bearer &lt;token>**"
        }
    }
}