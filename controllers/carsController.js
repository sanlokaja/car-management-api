const carsService = require("../services/carsService");

// ==================== End Get All Cars ====================//

const getAll = async (req, res) => {
    const {data} = await carsService.getAll();

    res.status(200).send({
        data: data,
    });
}

// ==================== End Get All Cars ====================//


// ==================== Create Car ====================//

const createCar = async (req, res, next) => {

    const createdBy = req.user.name;
    const updatedBy = req.user.name;
    const deletedBy = null;
    
    const {
        plate,
        manufacture,
        model,
        image,
        rentPerDay,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        availableAt,
        isWithDriver,
    } = req.body;

    const {status, status_code, message, data} = await carsService.createCar({
        plate,
        manufacture,
        model,
        image,
        rentPerDay,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        availableAt,
        isWithDriver,
        createdBy,
        updatedBy,
        deletedBy
    });

    res.status(status_code).send({
        status: status,
        message: message,
        data: data,
    });
}

// ==================== End Create Car ====================//


// ==================== Update Car ====================//

const updateCar = async (req, res, next) => {

    const {id} = req.params;
    const updatedBy = req.user.name;
    const deletedBy = null;

    const {
        plate,
        manufacture,
        model,
        image,
        rentPerDay,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        availableAt,
        isWithDriver
    } = req.body;

    

    const {status, status_code, message, data} = await carsService.updateCar({
        id,
        plate,
        manufacture,
        model,
        image,
        rentPerDay,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        availableAt,
        isWithDriver,
        updatedBy,
        deletedBy
    });

    res.status(status_code).send({
        status: status,
        message: message,
        data: data,
    });
}

// ==================== End Update Car ====================//


// ==================== Delete Car ====================//

const deleteCar = async (req, res, next) => {
    const { id } = req.params;
    const deletedBy = req.user.name;

    const {status, status_code, message, data} = await carsService.deleteCar({id, deletedBy});

    res.status(status_code).send({
        status: status,
        message: message,
        data: data,
    });

}

// ==================== End Delete Car ====================//


// ==================== Available Car ====================//

const availableCar = async (req, res, next) => {

    const {  isWithDriver, availableAt, capacity } = req.query;
    
    const { status, status_code, message, data } = await carsService.availableCar({isWithDriver, availableAt, capacity});

    res.status(status_code).send({
        status: status,
        message: message,
        data: data,
    });
}

// ==================== EndAvailable Car ====================//

module.exports = {getAll, createCar, updateCar, deleteCar, availableCar};