const bcrypt = require("bcrypt");
const SALT_ROUND = 10;

'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    const hashedPassword = await bcrypt.hash('lucas1234', SALT_ROUND);
    
    await queryInterface.bulkInsert('users', [{
      name: 'Lucas',
      email: 'lucas@gmail.com',
      password: hashedPassword,
      role: 'superadmin',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  async down (queryInterface, Sequelize) {

    await queryInterface.bulkDelete('users', null, {});
    
  }
};
