const usersRepository = require("../repositories/usersRepository");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { JWT } = require("../lib/const");
const { OAuth2Client } = require("google-auth-library");

const SALT_ROUND = 10;

class AuthService {

    // ========= Register ================ // 

    static async register({name, email, password, role}){
        try{
            // Payload Validation
            
            if(!name){
                return {
                    status: false,
                    status_code: 400,
                    message: "Nama wajib diisi",
                    data: {
                        registered_user: null,
                    },
                };
            }
            
            if(!email){
                return {
                    status: false,
                    status_code: 400,
                    message: "Email wajib diisi",
                    data: {
                        registered_user: null,
                    },
                };
            }

            if(!password){
                return {
                    status: false,
                    status_code: 400,
                    message: "Password wajib diisi",
                    data: {
                        registered_user: null,
                    },
                };
            }else if (password.length < 8){
                return {
                    status: false,
                    status_code: 400,
                    message: "Password minimal 8 karakter",
                    data: {
                        registered_user: null,
                    },
                };
            }

            
            if(!role){
                return {
                    status: false,
                    status_code: 400,
                    message: "Role wajib diisi",
                    data: {
                        registered_user: null,
                    },
                };
            }

            const getUser = await usersRepository.getByEmail({email});

            if(getUser){
                return {
                    status: false,
                    status_code: 400,
                    message: "Email sudah digunakan",
                    data: {
                        registered_user: null,
                    },
                };
            } else {
                const hashedPassword = await bcrypt.hash(password, SALT_ROUND);
                const createdUser = await usersRepository.create({
                    name,
                    email,
                    password: hashedPassword,
                    role,
                });

                return {
                    status: true,
                    status_code: 201,
                    message: `Berhasil mendaftarkan ${role} `,
                    data: {
                        registered_user: createdUser,
                    },
                };
            }
        } catch (err) {
            return {
                status: false,
                status_code: 500,
                message: err.message,
                data: {
                    registered_user: null,
                },
            };
        }
    }

    // ========= End Register ================ // 
    
    
    // ========= Login ================ // 

    static async login({email, password}){
        try{
            // Payload Validation
        
            if(!email){
                return {
                    status: false,
                    status_code: 400,
                    message: "Email wajib diisi",
                    data: {
                        registered_user: null,
                    },
                };
            }

            if(!password){
                return {
                    status: false,
                    status_code: 400,
                    message: "Password wajib diisi",
                    data: {
                        registered_user: null,
                    },
                };
            } else if (password.length < 8) {
                return {
                    status: false,
                    status_code: 400,
                    message: "Password minimal 8 karakter",
                    data: {
                        registered_user: null,
                    },
                };
            }

        const getUser = await usersRepository.getByEmail({email});

        if(!getUser){
            return {
                status: false,
                status_code: 404,
                message: "Email belum terdaftar",
                data: {
                    registered_user: null,
                },
            };
        } else {
            const isPasswordMatch = await bcrypt.compare(
                password,
                getUser.password
            );

            if(isPasswordMatch){
                const token = jwt.sign(
                    {
                        id: getUser.id,
                        email: getUser.email,
                    },
                    JWT.SECRET,
                    {
                        expiresIn: JWT.EXPIRED,
                    }
                );

                return {
                    status: true,
                    status_code: 200,
                    message: `User berhasil login`,
                    data: {
                        token,
                    },
                };
            } else {
                return {
                    status: false,
                    status_code: 400,
                    message: "Password salah",
                    data: {
                        user: null,
                    },
                };
            }
        }
        
        } catch (err){
            return {
                status: false,
                status_code: 500,
                message: err.message,
                data: {
                    user: null,
                },
            };
        }
    }

    static async loginGoogle({ google_credential: googleCredential }) {
        try {
          // Get google user credential
          const client = new OAuth2Client(
            "878652378412-rmjjckepnmtfqmn6b099tf95vsvmhuoh.apps.googleusercontent.com"
          );

          const userInfo = await client.verifyIdToken({
            idToken: googleCredential,
            audience:
              "878652378412-rmjjckepnmtfqmn6b099tf95vsvmhuoh.apps.googleusercontent.com",
          });

          const { email, name } = userInfo.payload;

          const getUserByEmail = await usersRepository.getByEmail({ email });

          if (!getUserByEmail) {
            await usersRepository.create({
              name,
              email,
              role: "user",
            });
          }

          const token = jwt.sign(
            {
              id: getUserByEmail.id,
              email: getUserByEmail.email,
            },
            JWT.SECRET,
            {
              expiresIn: JWT.EXPIRED,
            }
          );

          return {
            status: true,
            status_code: 200,
            message: "User berhasil login",
            data: {
              token,
            },
          };
        } catch (err) {
          return {
            status: false,
            status_code: 500,
            message: err.message,
            data: {
              registered_user: null,
            },
          };
        }
    }

    // ========= End Login ================ // 
}

module.exports = AuthService;