const carsRepository = require("../repositories/carsRepository");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { JWT } = require("../lib/const");

const SALT_ROUND = 10;

class CarsService {

    // ==================== Get All Cars ====================//

    static async getAll(){
        const getAllCars = await carsRepository.getAll();
        return {
            data: {
                get_all_cars: getAllCars,
            },
        };
    }

    // ==================== End Get All Cars ====================//


    // ==================== Create Car ====================//

    static async createCar({
        plate,
        manufacture,
        model,
        image,
        rentPerDay,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        availableAt,
        isWithDriver,
        createdBy,
        updatedBy,
        deletedBy
    }){

        try {
            // Payload Validation

            if(!plate){
                return {
                    status: false,
                    status_code: 400,
                    message: "Plate wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            if(!manufacture){
                return {
                    status: false,
                    status_code: 400,
                    message: "Manufacture wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            if(!model){
                return {
                    status: false,
                    status_code: 400,
                    message: "Model wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            if(!image){
                return {
                    status: false,
                    status_code: 400,
                    message: "Image wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            if(!rentPerDay){
                return {
                    status: false,
                    status_code: 400,
                    message: "Rent Per Day wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            if(!capacity){
                return {
                    status: false,
                    status_code: 400,
                    message: "Capacity wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            if(!description){
                return {
                    status: false,
                    status_code: 400,
                    message: "Description wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            if(!transmission){
                return {
                    status: false,
                    status_code: 400,
                    message: "Transmission wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            if(!type){
                return {
                    status: false,
                    status_code: 400,
                    message: "Type wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            if(!year){
                return {
                    status: false,
                    status_code: 400,
                    message: "Year wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            if(!options){
                return {
                    status: false,
                    status_code: 400,
                    message: "Options wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            if(!specs){
                return {
                    status: false,
                    status_code: 400,
                    message: "Specs wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            if(!availableAt){
                return {
                    status: false,
                    status_code: 400,
                    message: "Available at wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            if(isWithDriver === null){
                return {
                    status: false,
                    status_code: 400,
                    message: "Is with driver wajib diisi",
                    data: {
                        created_car: null,
                    },
                };
            }

            const createdCar = await carsRepository.createCar({
                plate,
                manufacture,
                model,
                image,
                rentPerDay,
                capacity,
                description,
                transmission,
                type,
                year,
                options,
                specs,
                availableAt,
                isWithDriver,
                createdBy,
                updatedBy,
                deletedBy
            });

            return {
                status: true,
                status_code: 201,
                message: `Berhasil menambahkan mobil`,
                data: {
                    created_car: createdCar,
                },
            };

        } catch (err) {
            return {
                status: false,
                status_code: 500,
                message: err.message,
                data: {
                    created_car: null,
                },
            };
        }
    }

    // ==================== End Create Car ====================//


    // ==================== Update Car ====================//


    static async updateCar({
        id,
        plate,
        manufacture,
        model,
        image,
        rentPerDay,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        availableAt,
        isWithDriver,
        updatedBy,
        deletedBy
    }){
        try{

            const getCarById = await carsRepository.getById({id});
            
            if(getCarById.id == id){
                const updatedCar = await carsRepository.updateCar({
                    id,
                    plate,
                    manufacture,
                    model,
                    image,
                    rentPerDay,
                    capacity,
                    description,
                    transmission,
                    type,
                    year,
                    options,
                    specs,
                    availableAt,
                    isWithDriver,
                    updatedBy,
                    deletedBy
                });

                return {
                    status: true,
                    status_code: 200,
                    message: `Data berhasil diubah`,
                    data: {
                        updated_car: updatedCar,
                    },
                };
            } else {
                return {
                    status: false,
                    status_code: 401,
                    message: `Resource Unauthorized`,
                    data: {
                        updated_car: null,
                    },
                };
            }

        } catch (err) {
            return {
                status: false,
                status_code: 500,
                message: err.message,
                data: {
                    updated_car: null,
                },
            };
        }
    } 

    // ==================== End Update Car ====================//


    // ==================== Delete Car ====================//

    static async deleteCar({id, deletedBy}){
        try{

            const getCarById = await carsRepository.getById({id});
            
            if(getCarById.id == id){
                const deletedCar = await carsRepository.deleteCar({id, deletedBy});

                return {
                    status: true,
                    status_code: 200,
                    message: `Data berhasil dihapus`,
                    data: {
                        deleted_car: deletedCar,
                    },
                };
            } else {
                return {
                    status: false,
                    status_code: 401,
                    message: `Resource Unauthorized`,
                    data: {
                        deleted_car: null,
                    },
                };
            }

        } catch (err) {
            return {
                status: false,
                status_code: 500,
                message: err.message,
                data: {
                    deleted_car: null,
                },
            };
        }
    }
    
    // ==================== End Delete Car ====================//


    // ==================== Available Car ====================//

    static async availableCar({isWithDriver, availableAt, capacity }){
        
        try{

            const getAvailableCars = await carsRepository.availableCar({ isWithDriver, availableAt, capacity });

            return {
                status: true, 
                status_code: 200,
                message: `Mobil yang tersedia hari ini`,
                data: {
                    get_available_cars: getAvailableCars,
                },
            };
            
        } catch (err) {
            return {
                status: false,
                status_code: 500,
                message: err.message,
                data: {
                    get_available_cars: null,
                },
            };
        }
    }

    // ==================== End Available Car ====================//
}

module.exports = CarsService;