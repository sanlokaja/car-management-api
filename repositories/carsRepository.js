const {cars} = require("../models");
const { Op } = require("sequelize");

class CarsRepository {

    static async getAll(){
        const getAllCars = await cars.findAll();
        return getAllCars;
    }

    static async availableCar({ isWithDriver, availableAt, capacity }){
        if(isWithDriver && availableAt && capacity ){

            const getAllCars = await cars.findAll({
                where:{ 
                    isWithDriver, 
                    availableAt: {
                        [Op.lte]: availableAt,
                    },
                    capacity,
                },
            });

            return getAllCars;
        }

        return cars;
    }

    static async getById({id}){
        const getCarById = await cars.findOne({
            where: {id}
        });

        return getCarById;
    }
    
    static async createCar({
        plate,
        manufacture,
        model,
        image,
        rentPerDay,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        availableAt,
        isWithDriver,
        createdBy,
        updatedBy,
        deletedBy
    }){

        const createdCar = cars.create({
            plate,
            manufacture,
            model,
            image,
            rentPerDay,
            capacity,
            description,
            transmission,
            type,
            year,
            options,
            specs,
            availableAt,
            isWithDriver,
            createdBy,
            updatedBy,
            deletedBy
        });

        return createdCar;
    }

    static updateCar({
        id,
        plate,
        manufacture,
        model,
        image,
        rentPerDay,
        capacity,
        description,
        transmission,
        type,
        year,
        options,
        specs,
        availableAt,
        isWithDriver,
        updatedBy,
        deletedBy
    }){

        const updatedCar = cars.update({
            plate,
            manufacture,
            model,
            image,
            rentPerDay,
            capacity,
            description,
            transmission,
            type,
            year,
            options,
            specs,
            availableAt,
            isWithDriver,
            updatedBy,
            deletedBy
        }, {
            where: {id}
        });

        return updatedCar;
    }

    static async deleteCar({id, deletedBy}){

        const deletedCar =  cars.destroy({
            where: {id},
        });

        return deletedCar;
    }

}

module.exports = CarsRepository;